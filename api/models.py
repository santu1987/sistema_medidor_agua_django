from django.db import models

class Usuarios(models.Model):
    id = models.AutoField(primary_key=True)
    nombres = models.CharField(max_length=300, blank=True)
    apellidos = models.CharField(max_length=300, blank=True)
    correo = models.CharField(max_length=300, blank=True)
    class Meta:
        db_table = u'usuarios'