from django.urls import path
from .views import UsuariosList

urlpatterns = [
    path('usuarios/', UsuariosList.as_view(), name='usuarios_list'),
]