from django.shortcuts import render
from rest_framework import generics
from .models import Usuarios
from .serializers import UsuariosSerializer

class UsuariosList(generics.ListCreateAPIView):
	queryset = Usuarios.objects.all()
	serializer_class = UsuariosSerializer