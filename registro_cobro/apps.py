from django.apps import AppConfig


class RegistroCobroConfig(AppConfig):
    name = 'registro_cobro'
