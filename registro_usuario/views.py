#-*- coding: utf-8 -*-
from django.shortcuts import render_to_response, HttpResponseRedirect, render, HttpResponse
from django.utils.encoding import smart_str
from sis_agua.settings import *


import hashlib
import json
import os, re
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.contrib.auth import logout
from django.db import connections
from datetime import datetime,timedelta
from datetime import *
from django.template import loader

# Create your views here.
def index(request):
	context = {
				"persona":""
	}
	template = loader.get_template('index.html')
	return render(request, 'index.html',context)