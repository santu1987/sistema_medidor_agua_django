var app = angular.module("SisAguaApp",['ui.bootstrap','ngCookies']);
	app.config(function($httpProvider,$interpolateProvider,$locationProvider) {
		//$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
		$httpProvider.defaults.xsrfCookieName = 'csrftoken';
	    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
		$interpolateProvider.startSymbol('{$');
		$interpolateProvider.endSymbol('$}');
		//$locationProvider.html5Mode(true);
	})


/*Cargar el perfectScrollBar solo agregando la calse scrollBarAvila*/
$(function(){
    [].forEach.call(document.querySelectorAll('.scrollBarAvila'), function (el) {
      Ps.initialize(el);
    });
})