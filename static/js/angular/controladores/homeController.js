	angular.module("SisAguaApp")
.controller("homeController", function($scope,$http,$location, $window,BienesFactory,DepartamentosFactory,personalFactory,EstadoBienFactory,bienesUsuariosFactory,crudMovimientoBienesFactory,datosBienesFactory){
	/*	variable Token a django para conectarse por ajax */	
	var csrftoken = getCookie('csrftoken');
	//---Cuerpo de metodos
	//--------------------------------------------------------
	$scope.iniciar_sesion = function(event){
		if($scope.validar_inicio(event)==true){
			//--
			var in_data = jQuery.param({'nombre_us':$scope.usuario.nombre_us, 'clave_us':$scope.usuario.clave_us});//'csrfmiddlewaretoken': csrftoken 
			//--Envio por post
			$http.post("/validar_usuario/",in_data)
				 .success(function(data, estatus, headers, config){
				//--
				 if(data["respuesta"]==true){
				 	$window.location.href = '/home/';
				 }else if(data["respuesta"]==false){
				 	swal("Ups!", "Error en datos de usuario!", "error");
				 }else{
				 	swal("Ups!", "Ocurrió un error inesperado", "error");
				 }
		  	   }).error(function(data,estatus){
					console.log(data);
			});
			//--
		}
	}
	//------------------------------------------------------
	$scope.iniciar_sesion_enter = function(event){
		if(event.keyCode === 13){
			event.preventDefault();
			$scope.iniciar_sesion();
		}
	}
	//------------------------------------------------------
	$scope.validar_inicio = function(event){
		if(!$scope.usuario.nombre_us){
			swal("Ups!", "Debe ingresar el nombre de usuario", "error");
			$("#nombre_us").focus();
			return false;	
		}else if(!$scope.usuario.clave_us){
			swal("Ups!", "Debe ingresar el password", "error");
			$("#clave_us").focus();
			return false;	
		}else{
			return true;
		}
	}
	//-----------------------------------------------------
});
