angular.module("SisAguaApp")
//---------------------------------------------------------------------------------------
//--Bloque de servicios
//--Servicio para cargar imagenes...	
.service('upload', ["$http", "$q", function ($http, $q) 
{
	this.uploadFile = function(file, url,nombre_archivo,id_bienes_detalles,csrftoken)
	{
		//alert(file);
		var deferred = $q.defer();
		var formData = new FormData();
		formData.append("file", file);
		formData.append("nombre_archivo",nombre_archivo);
		formData.append("id_bienes_detalles",id_bienes_detalles);
		return $http.post(url, formData, {
			headers: {
				"Content-type": undefined
			},
			transformRequest: angular.identity
		})
		.success(function(res)
		{
			deferred.resolve(res);
		})
		.error(function(msg, code)
		{
			deferred.reject(msg);
		})
		return deferred.promise;
	}	
}])
//--Bloque de factorias
.factory("mobiliarioFactory",['$http',function($http,csrftoken){
	return{
			asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
			},								
			cargar_mobiliario : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'estatus':estatus });
			$http.post("/mobiliarios/consultar_mobiliarios",in_data).success(callback);
		}
	}
}])
//Quitar cambio a edificación --
.factory("inmobiliarioFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_inmobiliario : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken,'estatus':estatus});
			$http.post("/inmobiliarios/consultar_inmobiliarios",in_data).success(callback);
		}
	}
}])

.factory("edificiacionFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_edificacion : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken,'estatus':estatus});
			$http.post("/inmobiliarios/consultar_inmobiliarios",in_data).success(callback);
		}
	}
}])

.factory("FormaAdquisicionFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_forma_adquisicion : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, "estatus":estatus });
			$http.post("/forma_adquisicion/consultar_forma_adquisicion",in_data).success(callback);
		}
	}
}])

.factory("EstatusUsoFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_estatus_uso : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, "estatus":estatus });
			$http.post("/estatus_uso/consultar_estatus_uso",in_data).success(callback);
		}
	}
}])

.factory("tiposCoberturasFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_tipos_coberturas : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, "estatus":estatus });
			$http.post("/tipos_coberturas/consultar_tipos_coberturas",in_data).success(callback);
		}
	}
}])
.factory("segurosFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_seguros : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, "estatus":estatus });
			$http.post("/seguros/consultar_seguros",in_data).success(callback);
		}
	}
}])
.factory("TiposSegurosFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_tipos_coberturas : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, "estatus":estatus });
			$http.post("/tipos_seguros/consultar_tipos_seguros",in_data).success(callback);
		}
	}
}])
.factory("unidadMedidaFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_unidad_medida : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, "estatus":estatus });
			$http.post("/unidad_medida/consultar_unidad_medida",in_data).success(callback);
		}
	}
}])
.factory("monedasFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_monedas : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, "estatus":estatus });
			$http.post("/monedas/consultar_monedas",in_data).success(callback);
		}
	}
}])
.factory("EstadoBienFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token, valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_estado_bien : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'estatus':estatus});
			$http.post("/estado_bien/consultar_estado_bien",in_data).success(callback);
		}
	}
}])

.factory("ColoresFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_colores : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, "estatus":estatus });
			$http.post("/colores/consultar_colores",in_data).success(callback);
		}
	}
}])

.factory("TiposDesincorporacionFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_colores : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, "estatus":estatus });
			$http.post("/tipos_desincorporacion/consultar_tipos_desincorporacion",in_data).success(callback);
		}
	}
}])
//--Cambio a unidad_adm
.factory("DepartamentosFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token, valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_departamentos : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'estatus':estatus });
			$http.post("/departamentos/consultar_departamentos",in_data).success(callback);
		}
	}	
}])

.factory("unidad_admFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token, valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_unidad_adm : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'estatus':estatus });
			$http.post("/departamentos/consultar_departamentos",in_data).success(callback);
		}
	}	
}])
.factory("coordinacionesFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token, valor_estatus, valor_departamento){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
											if(valor_departamento!="")
												departamento = valor_departamento;
											else
												departamento = "";
		},
		cargar_coordinaciones : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'estatus':estatus, 'departamento':departamento });
			$http.post("/coordinaciones/consultar_coordinaciones",in_data).success(callback);
		}
	}	
}])

.factory("personalFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token, valor_custodio){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_custodio!="")
												custodio = valor_custodio;
											else
												custodio = "";
		},
		cargar_personal : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'custodio':custodio });
			$http.post("/usuarios/consultar_personal",in_data).success(callback);
		}
	}	
}])
.factory("FranjasFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_franjas : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken , "estatus":estatus});
			$http.post("/franjas/consultar_franjas",in_data).success(callback);
		}
	}	
}])
.factory("MarcasFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_marcas : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken , "estatus":estatus});
			$http.post("/marcas/consultar_marcas",in_data).success(callback);
		}
	}	
}])
.factory("CrudAdminFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function(valor_token,data,valor_modulo){
										if(valor_token!=""){
											csrftoken = valor_token;
										}else{
											csrftoken = "";											
										}
										if(data != ""){
											datos = data;
										}else{
											datos = [];
										}
										if(valor_modulo != ""){
											modulo = valor_modulo;
										}else{
											modulo = "";
										}
		},
		guardar_valores : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken ,"datos":JSON.stringify(datos)});
			$http.post("/"+modulo+"/registroData",in_data).success(callback);
		}

	}
}])
//--Para consultas de lotes
.factory("BienesLotesFactory",['$http',function($http,csrftoken){
	return{
			asignar_valores : function(valor_token,data){
								if(valor_token!=""){
									csrftoken = valor_token;
								}else{
									csrftoken = "";											
								}
			},					
			cargar_valores : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken});
			$http.post("/incorporacion/consultar_bienes_lotes",in_data).success(callback);
		}

	}
}])
//--Para consultas de bienes no asignados
.factory("BienesNoAsignadosFactory",['$http',function($http,csrftoken){
	return{
			asignar_valores : function(valor_token,data,valor_tipo){
								if(valor_token!=""){
									csrftoken = valor_token;
								}else{
									csrftoken = "";											
								}
			},					
			cargar_valores : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken});
			$http.post("/incorporacion/consultas_bienes_no_asignados",in_data).success(callback);
		}

	}
}])
//--Para consultas de bienes no asignados(sin responsable de uso)
.factory("BienesFactory",['$http',function($http,csrftoken){
	return{
			asignar_valores : function(valor_token,data,valor_tipo){
								if(valor_token!=""){
									csrftoken = valor_token;
								}else{
									csrftoken = "";											
								}
								if(valor_tipo!=""){
										tipo = valor_tipo;
								}else{
										tipo = "";
								}
			},					
			cargar_valores : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken,'tipo':tipo});
			$http.post("/incorporacion/consultar_bienes",in_data).success(callback);
		}

	}
}])
//--Para consultas de partidas
.factory("partidasFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_valores : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken , "estatus":estatus});
			$http.post("/partidas/consultar_partidas",in_data).success(callback);
		}
	}
}])
//--Para consulta de origen
.factory("origenFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_valores : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken , "estatus":estatus});
			$http.post("/origen/consultar_origen",in_data).success(callback);
		}
	}
}])
//--Para consulta de proveedores
.factory("proveedoresFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_proveedores : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken , "estatus":estatus});
			$http.post("/proveedores/consultar_proveedores",in_data).success(callback);
		}
	}
}])
//--
//--Consulta de usuarios
.factory("usuariosFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function (valor_token,valor_estatus){
											if(valor_token!="")
												csrftoken = valor_token;
											else
												csrftoken = "";
											if(valor_estatus!="")
												estatus = valor_estatus;
											else
												estatus = "";
		},
		cargar_usuarios : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, "estatus":estatus });
			$http.post("/usuarios/consultar_usuarios_lista",in_data).success(callback);
		}
	}
}])
.factory("crudUsFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function(valor_token,data){
										if(valor_token!=""){
											csrftoken = valor_token;
										}else{
											csrftoken = "";											
										}
										if(data != ""){
											datos = data;
										}else{
											datos = [];
										}
		},
		guardar_valores : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken ,"datos":JSON.stringify(datos)});
			$http.post("/usuarios/registrar_usuario",in_data).success(callback);
		}

	}
}])
.factory("movimientosFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function(valor_token,valor_estatus, valor_tipo){
										if(valor_token!=""){
											csrftoken = valor_token;
										}else{
											csrftoken = "";											
										}
										if(valor_estatus!="")
											estatus = valor_estatus;
										else
											estatus = "";
										if(valor_tipo!="")
											tipo = valor_tipo;
										else
											tipo = "";
		},
		cargar_movimientos : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'estatus':estatus, 'tipo':tipo });
			$http.post("/bandeja_notificaciones/consultar_movimientos_generales",in_data).success(callback);
		}

	}
}])
.factory("movimientosExternosFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function(valor_token,valor_estatus){
										if(valor_token!=""){
											csrftoken = valor_token;
										}else{
											csrftoken = "";											
										}
										if(valor_estatus!="")
											estatus = valor_estatus;
										else
											estatus = "";
		},
		cargar_movimientos : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'estatus':estatus });
			$http.post("/bandeja_notificaciones/consultar_movimientos_externos",in_data).success(callback);
		}

	}
}])
.factory("movimientosGeneralesFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function(valor_token,valor_estatus,valor_tipo,valor_consulta){
										if(valor_token!=""){
											csrftoken = valor_token;
										}else{
											csrftoken = "";											
										}
										if(valor_estatus!="")
											estatus = valor_estatus;
										else
											estatus = "";
										if(valor_tipo!="")
											tipo = valor_tipo;
										else
											tipo = "";

										if(valor_consulta!="")
											consulta = valor_consulta;
										else
											consulta = "";
		},
		cargar_movimientos : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'estatus':estatus, 'tipo':tipo,"consulta":consulta });
			$http.post("/bandeja_notificaciones/consultar_movimientos_por_verificar",in_data).success(callback);
		}
	}
}])
.factory("verificarMovimientosGenerales",['$http',function($http,csrftoken){
	return{
		asignar_valores : function(valor_token,valor_tipo,valor_data,valor_rechazo){
										if(valor_token!=""){
											csrftoken = valor_token;
										}else{
											csrftoken = "";											
										}
										if(valor_tipo!="")
											tipo = valor_tipo;
										else
											tipo = "";
										if(valor_data!="")
											datos = valor_data;
										else
											datos = "";
										if(valor_rechazo != "")
											rechazo = valor_rechazo
										else
											rechazo = "";
		},
		verificar_movimientos : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'tipo':tipo,"datos":JSON.stringify(datos),"rechazo":rechazo});
			$http.post("/bandeja_notificaciones/verificar_movimientos_por_verificar",in_data).success(callback);
		}

	}
}])
.factory("bienesReparacion",['$http',function($http,csrftoken){
	return{
		asignar_valores : function(valor_token,valor_tipo,valor_data,valor_descripcion){
										if(valor_token!=""){
											csrftoken = valor_token;
										}else{
											csrftoken = "";											
										}
										if(valor_tipo!="")
											tipo = valor_tipo;
										else
											tipo = "";
										if(valor_data!="")
											datos = valor_data;
										else
											datos = "";
										if(valor_descripcion != "")
											descripcion = valor_descripcion
										else
											descripcion = "";
		},
		verificar_movimientos : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'tipo':tipo,"datos":JSON.stringify(datos),"mensajes":JSON.stringify(descripcion)});
			$http.post("/bandeja_notificaciones/bien_reparacion_event",in_data).success(callback);
		},

	}
}])
.factory("bienesUsuariosFactory",['$http',function($http,csrftoken){
	return{
		asignar_valores : function(valor_token,valor_cedula){
									if(valor_token!=""){
										csrftoken = valor_token;
									}else{
										csrftoken = "";
									}
									if(valor_cedula!=""){
										cedula = valor_cedula;
									}else{
										cedula = valor_cedula;
									}
		},
		cargar_bienes_usuarios : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'cedula':cedula });
			$http.post("/bienes_asignados/consultar_bienes_asignados",in_data).success(callback);
		}

	}
}])
.factory("TiposMovimientosExternosFactory",['$http', function($http,csrftoken){
	return{
		asignar_valores : function(valor_token,valor_estatus){
									if(valor_token!=""){
										csrftoken = valor_token;
									}else{
										csrftoken = "";
									}
									if(valor_estatus!=""){
										estatus = valor_estatus;
									}else{
										estatus = valor_estatus;
									}
		},
		cargar_tipos_movimientos : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'estatus':estatus });
			$http.post("/movimientos_externos/consultar_tipos_movimientos",in_data).success(callback);
		}
	}
}])
.factory("crudMovimientoBienesFactory",['$http', function($http,csrftoken){
	return{
		asignar_valores : function(valor_token,content){
									if(valor_token!=""){
										csrftoken = valor_token;
									}else{
										csrftoken = "";
									}
									if(content!=""){
										dta = content;
									}else{
										dta = [];
									}
		},
		guardar_valores : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'datos':JSON.stringify(dta)});
			$http.post("/home/registrarMovimientoBienes",in_data).success(callback);
		}
	}
}])
.factory("DesincorporacionFactory",['$http', function($http,csrftoken){
	return{
		asignar_valores : function(valor_token,valor_movimiento){
									if(valor_token!=""){
										csrftoken = valor_token;
									}else{
										csrftoken = "";
									}
									if(valor_movimiento!=""){
										movimiento = valor_movimiento;
									}else{
										movimiento = "";
									}
		},
		cargar_tipo_desincorporacion : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'movimiento':movimiento});
			$http.post("/por_desincorporar/consultar_desincorporacion",in_data).success(callback);
		}
	}
}])
.factory("tipoDesincorporacionFactory",['$http', function($http,csrftoken){
	return{
		asignar_valores : function(valor_token,valor_estatus){
									if(valor_token!=""){
										csrftoken = valor_token;
									}else{
										csrftoken = "";
									}
									if(valor_estatus!=""){
										estatus = valor_estatus;
									}else{
										estatus = valor_estatus;
									}
		},
		cargar_tipo_desincorporacion : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'estatus':estatus });
			$http.post("/por_desincorporar/consultar_tipo_desincorporacion",in_data).success(callback);
		}
	}
}])
.factory("datosBienesFactory",['$http', function($http,csrftoken){
	return{
		asignar_valores : function(valor_token,tipo,id_bien,pantalla){
									if(valor_token!=""){
										csrftoken = valor_token;
									}else{
										csrftoken = "";
									}
									if(tipo!=""){
										tipoBien = tipo;
									}else{
										tipoBien = "";
									}
									if(id_bien!=""){
										idbien = id_bien;
									}else{
										idbien = "";
									}
									if(pantalla!=""){
										pantallaBien = pantalla;
									}else{
										pantallaBien = "";
									}
		},
		caragr_datos_bienes : function(callback){
			var in_data = jQuery.param({'csrfmiddlewaretoken': csrftoken, 'tipo':tipoBien,'id_bien':idbien,"pantalla":pantallaBien});
			$http.post("/home/detalleBienesAsignados",in_data).success(callback);
		}
	}
}]);